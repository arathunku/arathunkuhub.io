function loadjscssfile(filename, filetype) {
  if (filetype == "js") {
    var fileref = document.createElement('script')
    fileref.setAttribute("type", "text/javascript")
    fileref.setAttribute("src", filename)
  } else if (filetype == "css") {
    var fileref = document.createElement("link")
    fileref.setAttribute("rel", "stylesheet")
    fileref.setAttribute("type", "text/css")
    fileref.setAttribute("href", filename)
  }

  if (typeof fileref != "undefined") {
    document.getElementsByTagName("head")[0].appendChild(fileref)
  }
}

function loadHighlightJS() {
  function initialize() {
    if (window.hljs) {
      hljs.initHighlighting();
    } else {
      setTimeout(function () {
        initialize();
      }, 256);
    }
  }

  if (document.querySelector("pre code")) {
    loadjscssfile(SITE.BaseUrl + "css/highlight.css", "css");
    loadjscssfile(SITE.BaseUrl + "js/highlight.min.js", "js");
    initialize();
  }
}

document.addEventListener("DOMContentLoaded", function(event) {
  loadHighlightJS();
});
